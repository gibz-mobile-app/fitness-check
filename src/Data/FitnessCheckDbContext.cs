using FitnessCheck.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace FitnessCheck.Data;

public class FitnessCheckDbContext : DbContext
{
    public virtual DbSet<DummyEntity> DummyEntities { get; set; }
    
    public FitnessCheckDbContext(DbContextOptions<FitnessCheckDbContext> options)
        : base(options)
    {
    }
}