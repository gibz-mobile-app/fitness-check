using FitnessCheck.Data;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Add JWT authorization
builder.Services.AddAuthentication().AddJwtBearer();

// Establish db connection
var connectionString = builder.Configuration["ConnectionStrings:MariaDb"];
var serverVersion = new MariaDbServerVersion(new Version(11, 2));
builder.Services.AddDbContextPool<FitnessCheckDbContext>(options => options.UseMySql(connectionString, serverVersion));


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapControllers();

app.Run();