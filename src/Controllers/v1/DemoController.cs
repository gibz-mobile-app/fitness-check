using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using FitnessCheck.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FitnessCheck.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class DemoController : Controller
{
    private readonly FitnessCheckDbContext _dbContext;

    public DemoController(FitnessCheckDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    [HttpGet]
    [Authorize]
    public async Task<IActionResult> TestEndpointAsync()
    {
        var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        var userEmail = User.FindFirstValue(ClaimTypes.Email);

        if (userId is not null && userEmail is not null)
        {
            await _dbContext.DummyEntities.AddAsync(new Data.Entities.DummyEntity
            {
                UserId = Guid.Parse(userId),
                Email = userEmail
            });
            await _dbContext.SaveChangesAsync();

            return Ok($"Successfully authenticated user: {userEmail} ({userId})");
        }

        return BadRequest($"At least one of these claims could not be retrieved: '{ClaimTypes.NameIdentifier}', '{ClaimTypes.Email}'");
    }

}