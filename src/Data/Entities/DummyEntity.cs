namespace FitnessCheck.Data.Entities;

public class DummyEntity
{
    public Guid Id { get; set; }
    public Guid UserId { get; set; }
    public string Email { get; set; } = null!;
}